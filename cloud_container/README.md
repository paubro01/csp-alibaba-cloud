
# Building and Deploying the Cloud container

## Pre-Requisites
- ARM Based Embedded Platform. The following are recommended:
    - Rockchip 3399ProD
    - NXP i.MX 8M Plus
- USB Pen Drive Flashed with ARM System-Ready EWAOL Image with preinstalled docker.
- An Alibaba Cloud Service account.
- Apsara VOD service activated for Alibaba account.
- Setup Link IoT Edge on target device. Refer to Link_iot_edge/README.

## Overview of the document
- Clone the repository
- Setup the Apsara Live service on Alibaba Cloud
- Setup the container
- Building the docker container
- Running the docker container with usb webcam
- Web UI guide

## Clone the repository
- Clone the gitlab repo using below command
```
$ git clone -b <BRANCH_NAME> <PROJECT_GIT_URL>
```
## Setup the Apsara Live service on Alibaba Cloud
Refer to the document at apsara_live/docs for setting the Apsara live and VOD service for Live streaming and recording.

## Setup the container
- Execute the command on the target device (e.g. Rockchip RK3399 or Imx8MP).
```sh
cd cloud_container/
```
- Open the config file **config/service_config.json** in an editor.
  - The Device key and Product key will be the same as the one used to set up Link IoT Edge service on the device. To find those again:
    - Go to the Alibaba cloud and search for **IoT Platform** on search bar.
    - Click on the **IoT Platform** from search results.
    - Select the preferred region and the device which was used to deploy **Link IoT Edge** on the local device.
    - On the top, click on **view** next to **DeviceSecret**.
    - Copy the Product Key and Device name.
    - Update the copied credentials in the config file.
  - For the **Cloud monitor group id**, assign a number.

    Note that all the cloud monitor metrics will be pushed to Alibaba cloud under this id. It will be used while setting up the dashboard on the Cloud monitor console.
  - For **Apsara VOD region** value, refer to the document at apsara_live/docs.
  - Save the config file.
- Open the config file **config/alibaba_creds.json** in an editor.
  - In the config file update the region, access_key and secret_key.
  - Save the file.
- In the config file **config/live_recording_config.json**.
  - Update the app_name, domain, stream_name, transcoding_id and max_cycle_duration.
    - app_name: The name of the application to which the live stream belongs.
    - domain: The main streaming domain.
    - stream_name: The name of the live stream.
    - transcoding_id: A code that is passed as a string. For generating the transcoding id, refer to the [Transcoding management](https://www.alibabacloud.com/help/en/apsaravideo-live/latest/transcoding-management) documentation.
    - max_cycle_duration: The duration of the periodically recorded video. Unit: seconds. Valid values: 300 to 21600. Default value: 3600.
  - Save the file.

## Building the docker container
- Change the working directory to the cloud contianer folder.
```sh
cd cloud_container/
```
- Build the docker container.
```sh
docker build -t <IMAGE_NAME>:<IMAGE_TAG> .
```
  Update IMAGE_NAME and IMAGE_TAG. These can be given according to user choice e.g. " cloud:i3 ".

- Check the Docker Images list with the command below.
```sh
docker images
```

## Running the docker container with usb webcam
- Run the docker container.
``` sh
docker run --network=host <IMAGE_NAME>:<TAG_NAME> -p <CLOUD_CONTAINER_PORT> -ls <LIVE_STREAMING_PUSH_URL_FROM_APSARA_VIDEO_LIVE>
```
- Arguments :
  - -p: Port number on which the flask server will be running. Must be different from other containers.
  - -ls : Live streaming ingest URL from Apsara Live service.

To obtain the Live streaming push URL, follow the steps mentioned in apsara_live/docs document.

## Web UI guide
The Web UI obtained from the application container will contain the **Video Management Server** button.

- The user can click this button to access the video management server. Otherwise, the user can open the flask url in the browser to directly navigate to the video management server.

Note: The application and cloud container will run on different ports.

- On the cloud-based video management server page.

  Note: The date and time displayed on this page will be according to the region you select to store videos on ApsaraVideo VOD. This Date and Time will not be the same as ApsaraVideo VOD console. For example, if the account is created for an Indian user and the user is using China (Shanghai) region to store videos: on Alibaba  ApsaraVideo VOD Cloud console, the user will see the Indian date and time. And on the ARMCSP video management server page, the user will see China time (Shanghai).

  - Click on any video to play it.
  - The user can get the videos uploaded in a time range by specifying start and
  end date time in the input box.
  - The user can delete videos individually, delete according to start and end date time, delete all previous videos (it will keep the latest 25 videos).
  - The user can use the time-based scheduling or object-based scheduling options to record videos as per their preferences on time to start recording, according to specified time range or preferred object.
  - Click on the **Time Schedule** button. The user can enter the **start date and time** and the **end date and time**. Then, click on the **Record** button. After this button is triggered, the videos will get recorded according to the specified time interval.

  - For object scheduling, click on the **Object Scheduling** button. A dropdown menu will appear. The user needs to select the object name (e.g., car or person or bus or truck etc.) according to their preferences and click on the **Record** button. As soon as this button is triggered, object-based recording will be started. The object-based recording will be started for further 2 mins as soon as the object is detected.
  - The video gets saved after 4 mins when the specified task is accomplished successfully. The user needs to wait till it is completed.

    Note that the internet must be stable, and the speed should be high. Sometimes, due to low internet speed, errors may occur.

## What's next,
- Refer to the README document of the Cloud Monitor folder.