import subprocess
import cv2
import numpy as np

# command and params for ffmpeg

class FFMPEG_SDK:
    def __init__(self, rtmp_url, width=640, height=480, fps=1.0):
        command = ['ffmpeg',
                    '-y',
                    '-f', 'rawvideo',
                    '-vcodec', 'rawvideo',
                    '-pix_fmt', 'bgr24',
                    '-s', "{}x{}".format(width,height),
                    '-r', str(fps),
                    '-i', '-',
                    '-c:v', 'libx264',
                    '-pix_fmt', 'yuv420p',
                    '-preset', 'ultrafast',
                    '-f', 'flv',
                    rtmp_url]

        
        self.pipeline = subprocess.Popen(command, stdin=subprocess.PIPE)

    def write_to_pipeline(self, img):
        img = np.asarray(bytearray(img), dtype='uint8')
        img = cv2.imdecode(img, cv2.IMREAD_COLOR)                                                                      
        self.pipeline.stdin.write(img.tobytes())
        
