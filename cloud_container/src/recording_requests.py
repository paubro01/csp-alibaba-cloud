import json
from aliyunsdklive.request.v20161101.AddLiveRecordVodConfigRequest import AddLiveRecordVodConfigRequest
from aliyunsdklive.request.v20161101.RealTimeRecordCommandRequest import RealTimeRecordCommandRequest
from aliyunsdklive.request.v20161101.DescribeLiveStreamStateRequest import DescribeLiveStreamStateRequest
import traceback
from aliyunsdkcore.acs_exception.exceptions import ClientException

class VodLiveRecording:
    def __init__(self, vod_client, domain, app_name, stream_name, transcoding_id, max_cycle_duration):
        self.domain = domain
        self.app_name = app_name
        self.stream_name = stream_name
        self.transcoding_id = transcoding_id
        self.max_cycle_duration = max_cycle_duration
        self.vod_client = vod_client
        
    def AddLiveRecordVodConfigRequest_function(self):
        try:
            config_request = AddLiveRecordVodConfigRequest()
            config_request.set_accept_format('json')
            config_request.set_DomainName(self.domain)
            config_request.set_AppName(self.app_name)
            config_request.set_StreamName(self.stream_name)
            config_request.set_VodTranscodeGroupId(self.transcoding_id)
            config_request.set_CycleDuration(self.max_cycle_duration)
            config_response = self.vod_client.do_action_with_exception(config_request) 
            print("Video recording configured", str(config_response, encoding='utf-8'))
            return config_response
        except ClientException as e:
            print("Connection error while configuring the stream")
            return None
        except Exception as e:
            print("Error while configuring stream", e)
            traceback.print_exc()

    #Forcefully stoping live recording
    def stop_command(self):
        try:
            stop_request = RealTimeRecordCommandRequest()
            stop_request.set_accept_format('json')
            stop_request.set_DomainName(self.domain)
            stop_request.set_AppName(self.app_name)
            stop_request.set_StreamName(self.stream_name)
            stop_request.set_Command("stop")
            stop_response = self.vod_client.do_action_with_exception(stop_request)
            print("Recording stoped...................", str(stop_response, encoding='utf-8'))
            return stop_response
        except ClientException as e:
            print("Connection error while stopping the stream")
            return None
        except Exception as e:
            print("Error while stopping stream")
            return None
        

    #Forcefully restarting live recording
    def start_command(self):
        try:
            restart_request = RealTimeRecordCommandRequest()
            restart_request.set_accept_format('json')
            restart_request.set_DomainName(self.domain)
            restart_request.set_AppName(self.app_name)
            restart_request.set_StreamName(self.stream_name)
            restart_request.set_Command("start")
            restart_response = self.vod_client.do_action_with_exception(restart_request) 
            #print("Recording restarted................", str(restart_response, encoding='utf-8'))
            return restart_response
        except ClientException as e:
            print("Connection error while trying to start stream")
            return None
        except Exception as e:
            print("Error while restarting stream")

    def is_live_stream_on(self):
        try:
            is_live_request = DescribeLiveStreamStateRequest()
            is_live_request.set_accept_format('json')
            is_live_request.set_AppName(self.app_name)
            is_live_request.set_StreamName(self.stream_name)
            is_live_request.set_DomainName(self.domain)
            is_live_response = self.vod_client.do_action_with_exception(is_live_request)
            is_live_response_var = json.loads(str(is_live_response, encoding='utf-8'))
            #print("Apsara Live status", str(is_live_response, encoding='utf-8'))
            return is_live_response_var['StreamState']
        except ClientException as e:
            print("Connection error while checking live streaming is on or off")
            return None
        except Exception as e:
            print("Error while checking status of stream", e)
            traceback.print_exc()
            return None
