# Deploying the containers on Kubernetes

## Introduction
This document provides step by step deployment of containers on target device (Rockchip RK3399 ProD or Imx8MP) using Kubernetes.

## Prerequisites
- Docker images for inference container, application container, video capturing container, cloud container and Cloud monitor container built and running without any errors.
- The built docker images must be pushed to docker hub.

To push a docker container:
- List the docker images.
```sh
docker images
```
- Tag the image of interest.
```sh
docker tag <IMAGE_OF_INTEREST_ID> <docker_hub_username>/<IMAGE_NAME>:<IMAGE_TAG>
```
- Make sure you are logged in with your docker hub account and push the image.
```sh
docker login
docker push <docker_hub_username>/<IMAGE_NAME>
```

## Overview
- Deployment of Edge-to-Edge pipeline.
- Deployment of Edge-to-Cloud pipeline.
- Getting logs from Kubernetes and opening Web UI.

Note: You can choose either to deploy Edge-to-Edge or Edge-to-Cloud. Choose the right deployment accordingly.

## Deployment of Edge-to-Edge pipeline.
- Open a terminal on the target device and navigate to the root of project directory and access the kubernetes folder.
```sh
cd project_root/kubernetes
```
- Open the yaml file **pod_Edge_Edge.yaml** in an editor.
    - Update the image name and tag name for all the containers and arguments given for the containers.
    - Save the yaml file.
- Execute the following command for deploying this configuration on kubernetes.
```sh
kubectl apply –f pod_Edge_Edge.yaml
```
- To get the logs of the pod execute the following command.
```sh
kubectl describe pod pod
```
- After getting all the containers as started. To get the container logs execute the following command. Provide the container name as given in pod file (-name <CONTAINER_NAME>).
```sh
kubectl logs -c <CONTAINER_NAME> pod
```

## Deployment of Edge-to-Cloud pipeline.
- Open a terminal on the target device and navigate to the root of project directory and access the kubernetes folder.
```sh
cd project_root/kubernetes
```
- Open the yaml file **pod_Edge_cloud.yaml** in an editor.
- Update the image name and tag name for all the containers and arguments given for the containers.
- Save the yaml file.
- Execute the following command for deploying this configuration on kubernetes.
```sh
kubectl apply –f pod_Edge_cloud.yaml
```
- To get the logs of the pod, execute the following command.
```sh
kubectl describe pod pod
```
- After getting all the containers as started. To get the container logs, execute the following command. Provide the container name as given in pod file (-name <CONTAINER_NAME>).
```sh
kubectl logs -c <CONTAINER_NAME> pod
```

Useful kubectl commands:
- To get pods.
    ```sh
    kubectl get pods
    ```
- To get all pods.
    ```sh
    kubectl get po -A
    ```
- To delete a pod.
    ```sh
    kubectl delete pod <pod_name>
    ```

## Getting logs from Kubernetes and opening Web UI.
- To get the flask link for Web UI execute the following command.
```sh
kubectl logs -c appcnt pod
```
- You will get a log similar to the following:
    ```
    Taking input from RTSP stream....
    * Serving Flask app 'main'
    * Debug mode: off
    WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
    * Running on all addresses (0.0.0.0)
    * Running on http://127.0.0.1:5000
    * Running on http://192.168.49.2:5000
    Press CTRL+C to quit
    ```
- Open the flask link similar to the one before the "CTRL+C" line on a web browser to get the Web UI.